require 'rest-client'

def send_sms(dest, text, real=false)
  """
  dest needs the country prefix like +30
  returns response in text form
  """
  sid = File.open('sid').readline().strip()
  puts 'a'+sid+'b'
  token = File.open('token').readline().strip()
  puts 'a'+token+'b'
  host = 'https://tadhack.restcomm.com'
  resource = RestClient::Resource.new(host, :user => sid, :password => token)
  endpoint = '/restcomm/2012-04-24/Accounts/' + sid + '/SMS/Messages.json'
  body = {
      'To': dest,
      'From': dest,
      'Body': text
  }
  result = 'No sms is sent at debug mode'
  if real
    response = resource[endpoint].post body
    result = response.body
  end
  return result
end

puts send_sms('+306909803267', 'Tell the guy next to you to stop spamming you...', true)


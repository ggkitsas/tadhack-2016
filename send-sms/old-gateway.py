import requests
from requests.auth import HTTPBasicAuth

def send_sms(src, dest, text, nocall=True):
    """
    src and dest are str and need the country prefix like +30
    returns response in text form
    """
    sid = open('sid').read().strip()
    token = open('token').read().strip()
    host = 'https://tadhack.restcomm.com' #'@tadhack.restcomm.com'
    endpoint = '/restcomm/2012-04-24/Accounts/' + sid + '/SMS/Messages.json'
    url = host + endpoint
    auth = (sid, token)
    body = {
        'To': dest,
        'From': src,
        'Body': text
    }
    if nocall:
        text = "No sms is sent in test mode"
    else:
        response = requests.post(url, body, auth=auth)
        assert response.status_code==200
        r = response.json()
        assert 'status' in r.keys(), 'No status key in ' + response.text
        assert r['status']=='sending', 'Status is not sending for ' + response.text
    return text

def main():
    print send_sms('+306989751727', '+306909803267', 'This is the fifth sms', False)

if __name__=='__main__':
    main()


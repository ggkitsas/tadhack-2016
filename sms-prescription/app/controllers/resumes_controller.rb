class ResumesController < ApplicationController
  def index
      @resumes = Resume.all
  end

  def new
      @resume = Resume.new
  end

  def create
      @resume = Resume.new(resume_params)

      if @resume.save
         redirect_to "/"
         line_num = 0
         msg = ""
         pharmacy_phone_num = ""
         params[:resume][:attachment].tempfile.each_line{|line| 
             if line_num == 0 then
               pharmacy_phone_num = line
             else
               prescription_info = line.split(',')
               msg += prescription_info[1] + " of " + prescription_info[0] + "\r\n"
             end
             line_num = line_num + 1
         }
         puts pharmacy_phone_num
         puts msg

         dest = pharmacy_phone_num
         text = msg
         real = true
         sid = "ACa86fceb2fd6a18cb1be8bd1411e61eb9"
         puts 'a'+sid+'b'
         token = "9bae503b4c213a9af0b998041af9f88f"
         puts 'a'+token+'b'
         host = 'https://tadhack.restcomm.com'
         resource = RestClient::Resource.new(host, :user => sid, :password => token)
         endpoint = '/restcomm/2012-04-24/Accounts/' + sid + '/SMS/Messages.json'
         body = {
             'To': dest,
             'From': dest,
             'Body': text
         }
         result = 'No sms is sent at debug mode'
         if real
           response = resource[endpoint].post body
           result = response.body
         end

         # Send the SMS
      else
         render "new"
      end
  end

  def destroy
      @resume = Resume.find(params[:id])
      @resume.destroy
      # redirect_to resumes_path, notice:  "The resume #{@resume.name} has been deleted."
   end

   private
      def resume_params
      params.require(:resume).permit(:name, :attachment)
   end

end

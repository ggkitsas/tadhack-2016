class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def upload_file
    uploaded_io = params[:person][:picture]
    File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename), 'wb') do |file|
      file.write(uploaded_io.read)
    end
  end

end


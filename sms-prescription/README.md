# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* System dependencies

Ubuntu 16.04

* Ruby version

Ruby version: 2.3.1

# based on http://ryanbigg.com/2014/10/ubuntu-ruby-ruby-install-chruby-and-you/
sudo apt-get install -y build-essentials

wget -O ruby-install-0.6.0.tar.gz   https://github.com/postmodern/ruby-install/archive/v0.6.0.tar.gz
tar -xzvf ruby-install-0.6.0.tar.gz
cd ruby-install-0.6.0/
sudo make install
cd ..
rm -rf ruby-install-0.6.0

mkdir ~/.rubies
ruby-install -i ~/.rubies/2.3.1 ruby 2.3.1

wget -O chruby-0.3.9.tar.gz   https://github.com/postmodern/chruby/archive/v0.3.9.tar.gz
tar -xzvf chruby-0.3.9.tar.gz
cd chruby-0.3.9/
sudo make install
cd ..
rm -rf chruby-0.3.9

cat >> ~/.zshrc <<EOF
source /usr/local/share/chruby/chruby.sh
source /usr/local/share/chruby/auto.sh
EOF
exec $SHELL
echo '2.3.1' > ~/.ruby-version

gem install bundler
bundle
gem install rails

* Configuration, DB creation and DB initialization

rails db:migrate RAILS_ENV=development

* How to run the test suite

N/A

* Deployment instructions

rails server

Rails.application.routes.draw do
   resources :resumes, only: [:index, :new, :create, :destroy]
   root "resumes#new"
end

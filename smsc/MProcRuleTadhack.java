/*
 * TeleStax, Open Source Cloud Communications  
 * Copyright 2012, Telestax Inc and individual contributors
 * by the @authors tag. See the copyright.txt in the distribution for a
 * full listing of individual contributors.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */

package org.mobicents.smsc.mproc.tadhack;

import javolution.xml.XMLFormat;
import javolution.xml.stream.XMLStreamException;

import org.mobicents.smsc.mproc.MProcMessage;
import org.mobicents.smsc.mproc.MProcRuleBaseImpl;
import org.mobicents.smsc.mproc.MProcRuleException;
import org.mobicents.smsc.mproc.MProcRuleRaProvider;
import org.mobicents.smsc.mproc.PostArrivalProcessor;

import java.net.HttpURLConnection;
import java.net.*;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.*;

/**
*
* @author sergey vetyutnev
*
*/
public class MProcRuleTadhack extends MProcRuleBaseImpl {

    @Override
    public String getRuleClassName() {
        return MProcRuleFactoryTadhack.CLASS_NAME;
    }


    private static final String PAR1 = "par1";
    private static final String PAR2 = "par2";
    private String par1, par2;

    @Override
    public void setInitialRuleParameters(String parametersString) throws Exception {
        String[] args = splitParametersString(parametersString);
        if (args.length != 2) {
            throw new Exception("parametersString must contains 2 parameters");
        }
        par1 = args[0];
        par2 = args[1];
    }

    @Override
    public void updateRuleParameters(String parametersString) throws Exception {
        String[] args = splitParametersString(parametersString);
        if (args.length != 2) {
            throw new Exception("parametersString must contains 2 parameters");
        }
        par1 = args[0];
        par2 = args[1];
    }

    @Override
    public String getRuleParameters() {
        return par1 + " " + par2;
    }


    @Override
    public boolean isForPostArrivalState() {
        return true;
    }


    @Override
    public boolean matchesPostArrival(MProcMessage message) {
        if (message.getDestAddr().startsWith(par1))
            return true;
        else
            return false;
    }

    @Override
    public boolean matchesPostImsiRequest(MProcMessage message) {
        return false;
    }

    @Override
    public boolean matchesPostDelivery(MProcMessage message) {
        return false;
    }

    @Override
    public void onPostArrival(final MProcRuleRaProvider anMProcRuleRa, PostArrivalProcessor factory, MProcMessage message)
            throws MProcRuleException {
        // String destAddr = this.par2 + message.getDestAddr();
        // factory.updateMessageDestAddr(message, destAddr);

		String url = ""; // TODO: raspberry pi IP and rest path which denotes the command
		String charset = "UTF-8";  // Or in Java 7 and later, use the constant: java.nio.charset.StandardCharsets.UTF_8.name()
		String param1 = ""; // TODO: command parameters
		String param2 = "";
		// ...
		try {
		  String query = String.format("param1=%s&param2=%s", 
		       URLEncoder.encode(param1, charset), 
		       URLEncoder.encode(param2, charset));


		  URLConnection connection = new URL(url).openConnection();
		  connection.setDoOutput(true); // Triggers POST.
		  connection.setRequestProperty("Accept-Charset", charset);
		  connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
		  try (OutputStream output = connection.getOutputStream()) {
		      output.write(query.getBytes(charset));
		  }
		  InputStream response = connection.getInputStream();

		} catch (IOException e) {
          // do whatever
        } finally {
        }

		
    }

    /**
     * XML Serialization/Deserialization
     */
    protected static final XMLFormat<MProcRuleTadhack> M_PROC_RULE_TEST_XML = new XMLFormat<MProcRuleTadhack>(
            MProcRuleTadhack.class) {

        @Override
        public void read(javolution.xml.XMLFormat.InputElement xml, MProcRuleTadhack mProcRule) throws XMLStreamException {
            M_PROC_RULE_BASE_XML.read(xml, mProcRule);

            mProcRule.par1 = xml.getAttribute(PAR1, "");
            mProcRule.par2 = xml.getAttribute(PAR2, "");
        }

        @Override
        public void write(MProcRuleTadhack mProcRule, javolution.xml.XMLFormat.OutputElement xml) throws XMLStreamException {
            M_PROC_RULE_BASE_XML.write(mProcRule, xml);

            xml.setAttribute(PAR1, mProcRule.par1);
            xml.setAttribute(PAR2, mProcRule.par2);
        }
    };

}

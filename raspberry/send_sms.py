import requests
from requests.auth import HTTPBasicAuth
import requests.packages.urllib3

def send_sms(src, dest, text, nocall=True):
    """
    src and dest are str and need the country prefix like +30
    returns response in text form
    """
    requests.packages.urllib3.disable_warnings()
    sid = open('sid').read().strip()
    token = open('token').read().strip()
    host = 'https://192.168.200.137:8443' #'@tadhack.restcomm.com'
    endpoint = '/restcomm/2012-04-24/Accounts/' + sid + '/SMS/Messages.json'
    url = host + endpoint
    auth = (sid, token)
    body = {
        'To': dest,
        'From': src,
        'Body': text
    }
    if nocall:
        text = "No sms is sent in test mode"
    else:
        response = requests.post(url, body, auth=auth, verify=False)
        assert response.status_code==200
        r = response.json()
        assert 'status' in r.keys(), 'No status key in ' + response.text
        assert r['status']=='sending', 'Status is not sending for ' + response.text
    return text


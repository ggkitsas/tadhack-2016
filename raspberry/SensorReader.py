from gpiozero import LEDBarGraph, MCP3008
import time

#Imort the send-sms function
#from send_sms import *
from enum import Enum

class WarningLevel(Enum):
    none = 0
    warning = 1
    critical = 2


def SendNotification(wl):
    #send_sms("stoMalaka","Warning Level" + WarningLevel,false)
    if wl == WarningLevel.warning:
        print "Warning"
    elif wl == WarningLevel.critical:
        print "Critical"


def CheckInterval(timestamp , wl):
    interval = 15.0
    if time.time() - timestamp > interval and wl > 0:
        #print "time is"
        #print time.time() - timestamp
        return True


graph=LEDBarGraph(6,13,19,26,pwm=True)
pot = MCP3008(channel=0)

WarningLevel = Enum('WarningLevel', 'none warning critical')
_warningLevel = WarningLevel.none
timestamp = time.time()

while True:
    rounded_value =  round(pot.value,3)*100
    graph.value = rounded_value

    if rounded_value < 40:
        timestamp = time.time()
        _warningLevel = WarningLevel.none
    elif rounded_value > 40 and rounded_value < 85:
        #if you come from lower W.L
        if _warningLevel == WarningLevel.none: # go up the warning level
            _warningLevel = WarningLevel.warning
            timestamp = time.time()
            SendNotification(_warningLevel)
        #Set the warning level
        _warningLevel = WarningLevel.warning

    else:
        if _warningLevel == WarningLevel.warning:
            _warningLevel = WarningLevel.critical
            SendNotification(_warningLevel)
            timestamp = time.time()
        _warningLevel = WarningLevel.critical


    if CheckInterval(timestamp, _warningLevel):
        SendNotification(_warningLevel)
        timestamp = time.time()

    time.sleep(0.3)


